import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as HighCharts from 'highcharts';
import jQuery from 'jquery';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {};

  ionViewDidLoad() {

    var allRooms = new Array();
    jQuery.getJSON('http://172.31.22.198:3000/getAllData', function(resp) {
      allRooms = resp.data;
       HighCharts.chart('container', {
               chart: {
                 type: 'pie',
                 zoomType: 'xy'
               },
               title: {
                   text: 'Intruders data'
               },
               series: [{
             name: 'Brands',
             colorByPoint: true,
             data: allRooms
         }]
       });
      });
  }
}
