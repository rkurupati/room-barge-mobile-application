import { Component } from '@angular/core';
import { NavController,ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { ModalContentPage } from '../../components/modal/modal';
import jQuery from 'jquery';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  public roomLists:Array;
  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {
    this.FetchRoomList();
  }

  FetchRoomList() {
    this.roomsList = [];
    var that = this;
    jQuery.getJSON('http://172.31.22.198:3000/roomList', function(resp) {
    that.roomsList = resp.roomList;
    });
  }

  itemSelected(item) {
    jQuery.ajax({
      type: "POST",
      url:"http://172.31.22.198:3000/getMacData",
      data:{
        macId : item.macId
      }
    });
    let modal = this.modalCtrl.create(ModalContentPage);
    modal.present();
  }
}
