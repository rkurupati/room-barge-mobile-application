import { NgModule } from '@angular/core';
import { NavigationComponent } from './navigation/navigation';
import { ModalContentPage } from './modal/modal';
@NgModule({
	declarations: [NavigationComponent,
    ModalContentPage],
	imports: [],
	exports: [NavigationComponent,
    ModalContentPage]
})
export class ComponentsModule {}
