import { Component } from '@angular/core';
import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import jQuery from 'jquery';

@Component({
  selector: 'modal',
  templateUrl: 'modal.html'
})
export class ModalContentPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
  )
    ionViewDidLoad() {

      var roomwise = new Array();
      jQuery.getJSON('http://172.31.22.198:3000/getMacData', function(resp) {
        roomwise = resp.data;
        console.log(roomwise);
         HighCharts.chart('roomWiseData', {
                 chart: {
                   type: 'pie',
                   zoomType: 'xy'
                 },
                 title: {
                     text: 'Intruders data'
                 },
                 series: [{
               name: 'Brands',
               colorByPoint: true,
               data: allRooms
           }]
         });
        });

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
